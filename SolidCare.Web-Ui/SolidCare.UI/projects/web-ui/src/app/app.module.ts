import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ChildcareHomeComponent } from './childcare-home/childcare-home.component';
import { HeaderComponent } from './base/header/header.component';
import { FooterComponent } from './base/footer/footer.component';
import { SideNavigationComponent } from './base/side-navigation/side-navigation.component';
import { ChildcareStepperComponent } from './childcare-home/childcare-stepper/childcare-stepper.component';

@NgModule({
  declarations: [
    AppComponent,
    ChildcareHomeComponent,
    HeaderComponent,
    ChildcareStepperComponent,
    SideNavigationComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
