import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildcareHomeComponent } from './childcare-home.component';

describe('ChildcareHomeComponent', () => {
  let component: ChildcareHomeComponent;
  let fixture: ComponentFixture<ChildcareHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildcareHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildcareHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
