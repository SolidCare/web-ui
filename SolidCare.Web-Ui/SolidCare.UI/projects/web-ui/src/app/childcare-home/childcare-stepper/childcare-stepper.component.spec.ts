import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildcareStepperComponent } from './childcare-stepper.component';

describe('ChildcareStepperComponent', () => {
  let component: ChildcareStepperComponent;
  let fixture: ComponentFixture<ChildcareStepperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChildcareStepperComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildcareStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
